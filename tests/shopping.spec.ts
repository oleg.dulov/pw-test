import { test, expect } from '@playwright/test';

/** 
test('pretest', async ({ page }) => {
  const [response] = await Promise.all([
    
    
    page.waitForResponse(
      "https://www.saucedemo.com/"
      ),
      // Triggers the response
      page.fill("input[name='user-name']", "standard_user"),
      page.fill("input[namr='password']", "secret_sauce"),
    
      page.click("button")
  ]);
  //return response.json();

  console.log('RESPONSE ' + await response.json());
});
*/

test('test', async ({ page, browserName }) => {
  await page.goto('https://www.saucedemo.com/');

  await page.locator('[data-test="username"]').click();
  await page.locator('[data-test="username"]').fill('standard_user');
  await page.locator('[data-test="username"]').press('Tab');
  await page.locator('[data-test="password"]').fill('secret_sauce');
  await page.locator('[data-test="password"]').press('Enter');
  await page.locator('[data-test="add-to-cart-sauce-labs-bike-light"]').click();
  await page.locator('[data-test="add-to-cart-sauce-labs-fleece-jacket"]').click();
  await page.locator('a').filter({ hasText: '2' }).click();

  await page.screenshot({ path: 'homepage-'+browserName+'.png', fullPage: true });

  await page.locator('[data-test="checkout"]').click();
  await page.locator('[data-test="firstName"]').click();
  await page.locator('[data-test="firstName"]').fill('aaa');
  await page.locator('[data-test="firstName"]').press('Tab');
  await page.locator('[data-test="lastName"]').fill('Dsssov');
  await page.locator('[data-test="lastName"]').press('Tab');
  await page.locator('[data-test="postalCode"]').fill('12345');
  await page.locator('[data-test="continue"]').click();
  await page.locator('[data-test="finish"]').click();
 


});