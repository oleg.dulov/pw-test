
import { addDays } from 'date-fns';

export class getWorkingDate {

  workingDays = {
    Sunday: false,
    Monday: true,
    Tuesday: true,
    Wednesday: true,
    Thursday: true,
    Friday: true,
    Saturday: false,
    
    at: function(n) { return this[Object.keys(this)[n]] }
  }


  format () {  
    var date = new Date();
    var dict = {};
    const browsers = ['chromium', 'firefox', 'webkit'];
    var lv_day_nr = 25

    for (const element of browsers) {
      /* console.log(element);
      console.log(" lv_day_nr " + lv_day_nr); */
      
      for (let step = lv_day_nr; step < lv_day_nr + 5; step++) {
        //console.log("date: " + date.setDate(date.getDate() + step) + "step: " + step);

        const newDate = addDays(date, step);
        //date.setDate(date.getDate() + step);

        /* console.log("step " + step); 
        console.log(date.toString());
        console.log(date.getDay()); */

        // Verifying whether a given day is available
        if(this.workingDays.at(newDate.getDay())) {
          /* console.log("WorkingDay: " + date.getDay()); */
          
          const year = newDate.getFullYear()
          const month = String(newDate.getMonth() + 1).padStart(2, '0')
          const day = String(newDate.getDate()).padStart(2, '0')
  
          dict[element] = `${day}.${month}.${year}`; //'10.08.2023'

          lv_day_nr = step + 1;

          break;
        }

      }

    }

      return dict;
  }
}

let myDate = new getWorkingDate(); 
console.log(myDate.format());

