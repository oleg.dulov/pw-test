"use strict";
exports.__esModule = true;
exports.getWorkingDate = void 0;
var date_fns_1 = require("date-fns");
var getWorkingDate = /** @class */ (function () {
    function getWorkingDate() {
        this.workingDays = {
            Sunday: false,
            Monday: true,
            Tuesday: true,
            Wednesday: true,
            Thursday: true,
            Friday: true,
            Saturday: false,
            at: function (n) { return this[Object.keys(this)[n]]; }
        };
    }
    getWorkingDate.prototype.format = function () {
        var date = new Date();
        var dict = {};
        var browsers = ['chromium', 'firefox', 'webkit'];
        var lv_day_nr = 25;
        for (var _i = 0, browsers_1 = browsers; _i < browsers_1.length; _i++) {
            var element = browsers_1[_i];
            /* console.log(element);
            console.log(" lv_day_nr " + lv_day_nr); */
            for (var step = lv_day_nr; step < lv_day_nr + 5; step++) {
                //console.log("date: " + date.setDate(date.getDate() + step) + "step: " + step);
                var newDate = (0, date_fns_1.addDays)(date, step);
                //date.setDate(date.getDate() + step);
                /* console.log("step " + step);
                console.log(date.toString());
                console.log(date.getDay()); */
                // Verifying whether a given day is available
                if (this.workingDays.at(newDate.getDay())) {
                    /* console.log("WorkingDay: " + date.getDay()); */
                    var year = newDate.getFullYear();
                    var month = String(newDate.getMonth() + 1).padStart(2, '0');
                    var day = String(newDate.getDate()).padStart(2, '0');
                    dict[element] = "".concat(day, ".").concat(month, ".").concat(year); //'10.08.2023'
                    lv_day_nr = step + 1;
                    break;
                }
            }
        }
        return dict;
    };
    return getWorkingDate;
}());
exports.getWorkingDate = getWorkingDate;
var myDate = new getWorkingDate();
console.log(myDate.format());
